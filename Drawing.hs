module Drawing where 

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Display

import DataTypes
import Sim

gridWidth = 700
gridHeight = 500

drawSimSettings :: SimSettings -> IO (Picture)
drawSimSettings sSet@(SimSettings args ss _ _ _) = return $ pictures $ (drawArgs sSet) : [drawSimState (rows args, cols args) ss]

drawSimState :: (Int, Int) -> SimState -> Picture
drawSimState (n, m) ss = (translate (-200) 0 . normalizePicture (n, m) . renderSimState) ss

drawArgs :: SimSettings -> Picture
drawArgs (SimSettings args ss round stop delay) = pictures $ [num, sim, nei, sta, res, ste, exi, sst]
  where
    num = (normalizeArg (200) . text) ("Round #: \t" ++ (show round) ++ "/" ++ (show $ maxRounds args))

    sim = (normalizeArg (80) . text) ("[Up] [Down] Similarity: \t" ++ (show $ threshold args))
    nei = (normalizeArg (40) . text) ("[Left] [Right] Radius: \t" ++ (show $ rad ss))

    sta = (normalizeArg (-40) . text) ("[Enter] Start / Stop")
    res = (normalizeArg (-80) . text) ("[R] Reset")
    ste = (normalizeArg (-120) . text) ("[S] Step")
    exi = (normalizeArg (-200) . text) ("[command + Q] Exit")

    stopStr = if stop then "Stopped" else "Playing"
    sst = (translate (-100) (-340) . scale 0.15 0.15 . text) ("Simulation State: " ++ stopStr)

normalizeArg :: Float -> Picture -> Picture
normalizeArg transVer oldPic = (translate (100) transVer . scale 0.1 0.1) oldPic


foldFunc :: (b -> a) -> ([a] -> b -> [a])
foldFunc f = (\aList b -> aList ++ [f b])

renderSimState :: SimState -> Picture
renderSimState ss = pictures (foldl (foldFunc rowToPictures) [] rowList)
  where
    City rowList = city ss

rowToPictures :: Row (House) -> Picture
rowToPictures row = pictures (foldl (foldFunc houseToPolygon) [] (houseList row))

houseToPolygon :: House -> Picture
houseToPolygon house = pictures [(color coloring . polygon) path, lineLoop path]
  where
    path = locationToPath $ location house
    coloring = determineColor $ state house

locationToPath :: Location -> Path
locationToPath (xInt, yInt) = [(x, y), (x + 1, y), (x + 1, y + 1), (x, y + 1)]
  where
    x = fromIntegral xInt
    y = fromIntegral yInt

determineColor :: State -> Color
determineColor (B Satisfied) = makeColor 0.0 0.0 1.0 1.0
determineColor (B Unsatisfied) = makeColor 0.0 0.0 1.0 0.5
determineColor (R Satisfied) = makeColor 1.0 0.0 0.0 1.0
determineColor (R Unsatisfied) = makeColor 1.0 0.0 0.0 0.5
determineColor O = white

normalizePicture :: (Int, Int) -> Picture -> Picture
normalizePicture (maxXInt, maxYInt) oldPic = (rotate 90.0 . scale scaleFactor scaleFactor . translate transHor transVer) oldPic
  where
    (maxX, maxY) = (fromIntegral maxXInt, fromIntegral maxYInt)
    (minX, minY) = (0.0, 0.0)
    (xDist, yDist) = (maxX - minX, maxY - minY)
    (transHor, transVer) = (-(minX + xDist / 2), -(minY + yDist / 2))
    scaleFactor = determineScale xDist yDist

determineScale :: Float -> Float -> Float
determineScale xDist yDist
  | scaleX < scaleY = scaleX
  | otherwise = scaleY
  where
    scaleX = (fromIntegral gridWidth) / xDist
    scaleY = (fromIntegral gridHeight) / yDist