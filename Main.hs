module Main where 

import Text.Read
import System.Random
import System.Environment

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Display
import Graphics.Gloss.Interface.IO.Game as Game

import DataTypes
import Sim
import Drawing
--Window
windowWidth = 1024
windowHeight = 768

window :: Display
window = InWindow "Schelling's Model" (windowWidth, windowHeight) (10, 10)

fps :: Int 
fps = 60 

main :: IO()
main = do
  args <- getArgs
  chooseArgs args

initArgs :: Int -> Int -> Int -> Int -> Int -> ArgsPackage
initArgs mr gs rp bp emp = ArgsPackage {
  maxRounds = mr, 
  rows = gs, 
  cols = gs, 
  neighRadius = 1, 
  threshold = 0.44, 
  redPer = (fromIntegral rp/100), 
  bluePer = (fromIntegral bp/100), 
  emptyPer = (fromIntegral emp/100)}

chooseArgs :: [String] -> IO ()
chooseArgs (mr : "-f" : fileName : rp : bp : emp :[]) = do
  gridData <- readFile fileName
  let gridDataLines = lines gridData
  let initSimSettings = parseGrid gridDataLines (parseInt mr)

  playIO window white fps initSimSettings drawSimSettings eventHandler stepFunc

chooseArgs (mr : grid : rp : bp : emp : []) = do
  (gen, gen') <- getGens
  if((parseInt emp) > 100) || ((parseInt rp) + (parseInt bp) /= 100) then do
    putStrLn $ "Invalid input parameters!"
  else do
    let argsPackage = initArgs (parseInt mr) (parseInt grid) (parseInt rp) (parseInt bp) (parseInt emp)
        fullSS = initState argsPackage gen gen'
        initSimSettings = SimSettings{argsPack = argsPackage, simState = fullSS, roundNum = 0, stopped = True, stepDelay = 1.0}

    playIO window white fps initSimSettings drawSimSettings eventHandler stepFunc

chooseArgs _ = do
  putStrLn $ "Usage: ./Main max_steps [grid_size || -f grid.txt] red_percentage blue_percentage empty_percentage"

initState :: ArgsPackage -> StdGen -> StdGen -> SimState
initState (ArgsPackage mr rowNum colNum neighRadius threshold redPer bluePer emptyPer) gen gen' = fullSS
  where
    emptySS = initSimState (rowNum, colNum) neighRadius threshold
    totalPlots = rowNum * colNum

    (blueHouses, redHouses) = detBlueRed totalPlots emptyPer (bluePer, redPer)
    halfSS = moveInPeople blueHouses (B Satisfied) gen emptySS
    fullSS = moveInPeople redHouses (R Satisfied) gen' halfSS

eventHandler :: Event -> SimSettings -> IO (SimSettings)
-- Start / Stop
eventHandler (EventKey (SpecialKey KeyEnter) Up _ _) sSet = return $ sSet{stopped = (not . stopped) sSet}
-- Reset
eventHandler (EventKey (Game.Char 'r') Up _ _) sSet = resetState sSet (argsPack sSet)
-- Step
eventHandler (EventKey (Game.Char 's') Up _ _) sSet@(SimSettings _ ss round _ _) = 
  return $ sSet{simState = updateSimState ss, roundNum = round + 1}
-- Change Similarity Threshold
eventHandler (EventKey (SpecialKey KeyUp) Up _ _) sSet = changeThresh (thresholdStep) sSet
eventHandler (EventKey (SpecialKey KeyDown) Up _ _) sSet = changeThresh (-thresholdStep) sSet
-- Change Neighborhood Radius
eventHandler (EventKey (SpecialKey KeyLeft) Up _ _) sSet = changeRad (radiusStep) sSet
eventHandler (EventKey (SpecialKey KeyRight) Up _ _) sSet = changeRad (-radiusStep) sSet
-- Invalid Input
eventHandler _ sSet = return sSet

resetState :: SimSettings -> ArgsPackage -> IO (SimSettings)
resetState sSet newArgs = do
  (gen, gen') <- getGens
  return $ sSet{simState=initState newArgs gen gen', argsPack = newArgs, roundNum = 0, stopped = True, stepDelay = 1.0}

getGens :: IO (StdGen, StdGen)
getGens = do
  gen <- getStdGen 
  gen' <- newStdGen
  return (gen, gen')
-- Range
thresholdRange = (0.0, 1.0)
radiusRange = (0, 5)
delayRange = (0.5, 3.0)
redBlueRange = (0.0, 1.0)
emptyRange = (0.0, 1.0)
sizeRange = (10, 50)
-- Step
thresholdStep = 0.05
radiusStep = 1
delayStep = 0.100
redBlueStep = 0.05
emptyStep = 0.05

changeThresh :: Float -> SimSettings -> IO (SimSettings)
changeThresh change sSet@(SimSettings args ss _ _ _) = do
  let newThresh = changeFloat (threshold args) change thresholdRange
  return sSet{argsPack = args{threshold = newThresh}, simState = ss{thresh = newThresh}}

changeFloat :: Float -> Float -> (Float, Float) -> Float
changeFloat old change (min, max) = if old + change > max then max else if old + change < min then min else old + change

changeRad :: Int -> SimSettings -> IO (SimSettings)
changeRad change sSet@(SimSettings args ss _ _ _) = do
  let newRadius = changeInt (neighRadius args) change radiusRange
  return sSet{argsPack = args{neighRadius = newRadius}, simState = ss{rad = newRadius}}

changeInt :: Int -> Int -> (Int, Int) -> Int
changeInt old change (min, max) = if old + change > max then max else if old + change < min then min else old + change

stepFunc :: Float -> SimSettings -> IO (SimSettings)
stepFunc deltaTime sSet@(SimSettings args ss round stop delay) =
  if round >= (maxRounds args) || stop then return sSet{stopped = True} else
  if eTime' > delay 
    then return $ checkSim sSet
    else return $ sSet{simState = changeTimer eTime' ss}
  where
    eTime' = (elaspedTime ss) + deltaTime

checkSim :: SimSettings -> SimSettings
checkSim oldSSet@(SimSettings args ss round stop _) = 
  if city ss == (city . simState) newSSet
    then sameSSet
    else newSSet
  where
    newSSet = oldSSet{simState = (changeTimer 0.0 . updateSimState) ss, roundNum = round+1}
    sameSSet = oldSSet{stopped = True}

changeTimer :: Float -> SimState -> SimState
changeTimer newTime ss = ss{elaspedTime = newTime}
--Parsing
parseInt :: String -> Int 
parseInt str = read str :: Int

parseGrid :: [String] -> Int -> SimSettings
parseGrid (rowStr : colStr : rest) mr =
  let
    rowNum = parseInt rowStr
    colNum = parseInt colStr
    emptyState = initSimState (rowNum, colNum) 1 0.44
    (filledState, sqCount) = parseRows rest (0, 0) (emptyState, (0, 0, 0))
    (rPer, bPer, oPer) = toPercent sqCount

    argsPackage = ArgsPackage{maxRounds = mr, rows = rowNum, cols = colNum, neighRadius = 1, threshold = 0.44, emptyPer = oPer, redPer = rPer, bluePer = bPer}
  in SimSettings{argsPack = argsPackage, simState = filledState, roundNum = 0, stopped = True, stepDelay = 1.0}

parseRows :: [String] -> Location -> (SimState, (Int, Int, Int)) -> (SimState, (Int, Int, Int))
parseRows [] _ oldSS = oldSS
parseRows (row:rows) (rowPtr, colPtr) oldSS =
  let
    newSS = parseCols (words row) (rowPtr, colPtr) oldSS
  in (parseRows rows (rowPtr+1, colPtr) newSS)

parseCols :: [String] -> Location -> (SimState, (Int, Int, Int)) -> (SimState, (Int, Int, Int))
parseCols [] _ oldSS = oldSS
parseCols (house:houses) (rowPtr, colPtr) (oldSS, oldCount) =
  let
    (houseState, newCount) = parseHouse house oldCount
    newSS = if houseState /= O then moveIn (rowPtr, colPtr) houseState oldSS else oldSS
  in (parseCols houses (rowPtr, colPtr+1) (newSS, newCount))

parseHouse :: String -> (Int, Int, Int) -> (State, (Int, Int, Int))
parseHouse "R" (r, b, o) = (R Satisfied, (r+1, b, o))
parseHouse "B" (r, b, o) = (B Satisfied, (r, b+1, o))
parseHouse "O" (r, b, o) = (O, (r, b, o+1))
parseHouse _ _ = error $ "Invalid satifaction state in the grid!"

toPercent :: (Int, Int, Int) -> (Float, Float, Float)
toPercent (r, b, o) = (rPer, bPer, oPer)
  where
    total = r + b + o
    noEmpty = r + b

    rPer = (fromIntegral r) / (fromIntegral noEmpty)
    bPer = (fromIntegral b) / (fromIntegral noEmpty)
    oPer = (fromIntegral o) / (fromIntegral total)

initSimState :: (Int, Int) -> Int -> Float -> SimState
initSimState (rows, cols) rad thresh = SimState {city = initCity rows cols, openHomes = initOpenHomes rows cols, rad = rad, thresh = thresh, elaspedTime = 0.0}

initCity :: Int -> Int -> City (Row (House))
initCity rows cols = City [initRow rowNum cols | rowNum <- [0..(rows-1)]]

initRow :: Int -> Int -> Row (House)
initRow rowNum cols = Row {rowNum = rowNum, houseList = [House {location = (rowNum, colNum), state = O} | colNum <- [0..(cols-1)]]}

initOpenHomes :: Int -> Int -> OpenHomes (Location)
initOpenHomes rows cols = OpenHomes [(rowNum, colNum) | rowNum <- [0..(rows-1)], colNum <- [0..(cols-1)]]

detBlueRed :: Int -> Float -> (Float, Float) -> (Int, Int)
detBlueRed t ePer (bPer, _) = (b, r)
  where
    a = (round . (*) ((-) (1.0) ePer) . fromIntegral) t
    b = (round . (*bPer) . fromIntegral) a
    r = (-) a b