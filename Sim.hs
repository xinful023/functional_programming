module Sim where 

import System.Random

import DataTypes
--City Initialization
getHouse :: Location -> City (Row (House)) -> House
getHouse loc@(row, col) (City rowList) = ((houseList r) !! col)
  where
    r = (rowList !! row)

changeHouseState :: Location -> State -> House -> House
changeHouseState loc newState house = if (location house) == loc then house {state = newState} else house

changeState :: Location -> State -> Row (House) -> Row (House)
changeState loc newState oldRow = fmap (changeHouseState loc newState) oldRow

removeFromOpenHomes :: Location -> OpenHomes (Location) -> OpenHomes (Location)
removeFromOpenHomes loc (OpenHomes locationList) = OpenHomes (filter (/= loc) locationList)

addToOpenHomes :: Location -> OpenHomes (Location) -> OpenHomes (Location)
addToOpenHomes loc (OpenHomes locationList) = OpenHomes (locationList ++ [loc])

moveIn :: Location -> State -> SimState -> SimState
moveIn loc st ss = ss {city = newCity, openHomes = newOpenHomes}
  where
    newCity = fmap (changeState loc st) (city ss)
    newOpenHomes = removeFromOpenHomes loc (openHomes ss)

pickRandLoc :: Int -> OpenHomes (Location) -> Location
pickRandLoc randNum (OpenHomes l)= l !! (randNum `mod` (length l))

moveInPeople :: Int -> State -> StdGen -> SimState -> SimState
moveInPeople 0 _ _ ss = ss
moveInPeople num st gen oldSS = moveInPeople (num - 1) st gen' newSS
  where
    (randNum, gen') = random gen :: (Int, StdGen)
    loc = pickRandLoc randNum (openHomes oldSS)
    newSS = moveIn loc st oldSS
--Count Percentage
isSatisfied :: (Float, Float) -> State -> (Float, Float)
isSatisfied (sat, nonSat) s
  |s == (B Satisfied) || s == (R Satisfied) = (sat + 1, nonSat)
  |s == (B Unsatisfied) || s == (R Unsatisfied) = (sat, nonSat + 1)
  |otherwise = (sat, nonSat)

countSatisfaction :: (Float, Float) -> House -> (Float, Float)
countSatisfaction sats house = isSatisfied sats (state house)

getSatisfied :: SimState -> Float
getSatisfied ss = sat / (sat + nonSat) * 100
  where
    (sat, nonSat) = foldl (\sum rows -> foldl countSatisfaction sum rows) (0.0, 0.0) (city ss)
--Update City
updateSimState :: SimState -> SimState
updateSimState oldSS = foldl traverseRows oldSS (city oldSS)

traverseRows :: SimState -> Row (House) -> SimState
traverseRows oldSS singleRow = foldl traverseCols oldSS singleRow

traverseCols :: SimState -> House -> SimState
traverseCols oldSS oldHouse = (checkSatisfaction . flipHouseState oldSS) oldHouse

checkSatisfaction :: (House, SimState) -> SimState
checkSatisfaction (updatedHouse@(House l (B Unsatisfied)), updatedSS) = checkNewLocations updatedHouse updatedSS
checkSatisfaction (updatedHouse@(House l (R Unsatisfied)), updatedSS) = checkNewLocations updatedHouse updatedSS
checkSatisfaction (_, updatedSS) = updatedSS

checkNewLocations :: House -> SimState -> SimState
checkNewLocations updatedHouse updatedSS = let maybeNewLoc = attendAOpenHome updatedSS updatedHouse in
  if maybeNewLoc /= Nothing then swapLocations (location updatedHouse) (maybeNewLoc) updatedSS else updatedSS

swapLocations :: Location -> Maybe (Location) -> SimState -> SimState
swapLocations oldLoc (Just newLoc) oldSS = oldSS {city = newCity, openHomes = newOpenHomes}
  where
    oldState = state $ getHouse oldLoc (city oldSS)
    newState = if oldState == B Unsatisfied then B Satisfied else R Satisfied

    newCity = fmap (changeState oldLoc O . changeState newLoc newState) (city oldSS) 
    newOpenHomes = (addToOpenHomes oldLoc . removeFromOpenHomes newLoc) (openHomes oldSS)

attendAOpenHome :: SimState -> House -> Maybe (Location)
attendAOpenHome ss h = fst $ foldl (attendAOpenHome' ss h) (Nothing, 10.0) (openHomes ss)

attendAOpenHome' :: SimState -> House -> (Maybe Location, Float) -> Location -> (Maybe Location, Float)
attendAOpenHome' ss h (mLoc, minSim) x = 
  if simScore >= t && simScore < minSim 
    then (Just x, simScore)
    else (mLoc, minSim)
  where 
    t = thresh ss
    simScore = similarityScore ss h x

flipHouseState :: SimState -> House -> (House, SimState)
flipHouseState oldSS oldHouse@(House _ O) = (oldHouse, oldSS)
flipHouseState oldSS oldHouse@(House houseLoc houseState) = (updatedHouse, updatedSS)
  where
    t = thresh oldSS
    simScore = similarityScore oldSS oldHouse houseLoc
    updatedState = if simScore < t then (changeSatisfaction houseState) else houseState
    updatedHouse = oldHouse{state = updatedState}
    updatedCity = fmap (changeState houseLoc updatedState) (city oldSS)
    updatedSS = oldSS{city = updatedCity}

changeSatisfaction :: State -> State
changeSatisfaction (B Satisfied) = (B Unsatisfied)
changeSatisfaction (R Satisfied) = (R Unsatisfied)
changeSatisfaction s = s
--Similarity Score
similarityScore :: SimState -> House -> Location -> Float
similarityScore ss h@(House l compState) loc = numerator / denominator
  where
    Neighborhood n = getNeighborhood loc ss
    f = isInNeighborhood l (Neighborhood n)
    numerator = (fromIntegral . f . length) $ filter (isSameState compState . state) n
    denominator = (fromIntegral . f . length) $ filter (isOccupied . state) n

getNeighborhood :: Location -> SimState -> Neighborhood (House)
getNeighborhood (row, col) ss = Neighborhood houses
  where
    r = (rad ss)
    City rowList = city ss

    rows = filter (checkRange row r . rowNum) rowList
    houses = foldl (\accum x -> accum ++ (filter (checkRange col r . snd . location) $ houseList x)) [] rows

checkRange :: Int -> Int -> Int -> Bool
checkRange base radius test = if test >= (base - radius) && test <= (base + radius) then True else False

isSameState :: State -> State -> Bool
isSameState (B _) (B _) = True
isSameState (R _) (R _) = True
isSameState _ _ = False

isOccupied :: State -> Bool
isOccupied O = False
isOccupied _ = True

isInNeighborhood :: Location -> Neighborhood (House) -> (Int -> Int)
isInNeighborhood l (Neighborhood n) = if(foldl (\accum h -> accum || (l == (location h))) False n) then (+0) else (+1)