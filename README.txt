In Homework #6, I implement a Gloss application of Schelling’s Model.

Usage:
./Main max_steps [grid_size || -f grid.txt] red_percentage blue_percentage empty_percentage

I tested the example in Assignment Overview section on the website using the following command.

Xinfus-MacBook-Pro:hw6 fu$ ghc Main.hs 
Xinfus-MacBook-Pro:hw6 fu$ ./Main 10 -f grid.txt 50 50 20

I check the output step by step. I am very happy that it gives the exact same result as described in Assignment Overview section.
I also check all the required functions. All of them work as expected.

The key assignments are as follows:

Threshold: Up arrow key (increasing +5), Down arrow key (decreasing -5)
Step request: 's' key
Reset request: 'r' key
R (neighbor size): Left arrow key (increasing +1), Right arrow key (decreasing -1).

I did all the following subtasks:

1. compute a homeowner’s similarity score;
2. create and initialize the list of open locations;
3. swap the values at two locations;
4. evaluate the open locations as potential new homes for a homeowner;
5. simulate one step of the simulation;
6. run steps until one of the stopping conditions are met (i.e., by the user requesting to stop or you have reached the threshold satification value).
7. implement the -f optional flag

I do not use a lot comments in the code since my Applied Software Engineering lecturer tells us a good variable/function/class name will tell everything.

I read a lot of code on GitHub and also discussed with my classmates on this cool project.