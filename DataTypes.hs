module DataTypes where 

import Data.Foldable

type Location = (Int, Int)
-- City
data City a = City [a] deriving (Eq)
instance Functor City where
  fmap f (City []) = City []
  fmap f (City a) = City (fmap f a)

instance Applicative City where
  pure = (City . pure [])
  (City [a]) <*> (City [b]) = (City ([a] <*> [b]))

instance Monad City where
  (>>=) (City [a]) f = f a

instance Monoid (City a) where
  mempty = City []
  (City listA) `mappend` (City listB) = City (listA `mappend` listB)

instance Foldable City where
  foldMap f (City []) = mempty
  foldMap f (City list) = foldMap f list
-- Row
data Row a = Row {rowNum :: Int, houseList :: [a]} deriving (Eq)
instance Functor Row where
  fmap f (Row rowNum []) = Row rowNum []
  fmap f (Row rowNum a) = Row rowNum (fmap f a)

instance Applicative Row where
  pure = (Row 0 . pure [])
  (Row _ [b]) <*> (Row rowNum [a]) = (Row rowNum ([b] <*> [a]))

instance Monad Row where
  (>>=) (Row rowNum [a]) f = f a

instance Monoid (Row a) where
  mempty = Row 0 []
  (Row rowNum listA) `mappend` (Row _ listB) = Row rowNum (listA `mappend` listB)

instance Foldable Row where
  foldMap f (Row _ []) = mempty
  foldMap f (Row rowNum list) = foldMap f list
-- Neighborhood
data Neighborhood a = Neighborhood [a] deriving (Eq)
instance Functor Neighborhood where
  fmap f (Neighborhood []) = Neighborhood []
  fmap f (Neighborhood a) = Neighborhood (fmap f a)

instance Applicative Neighborhood where
  pure = (Neighborhood . pure [])
  (Neighborhood [a]) <*> (Neighborhood [b]) = (Neighborhood ([a] <*> [b]))

instance Monad Neighborhood where
  (>>=) (Neighborhood [a]) f = f a

instance Monoid (Neighborhood a) where
  mempty = Neighborhood []
  (Neighborhood listA) `mappend` (Neighborhood listB) = Neighborhood (listA `mappend` listB)

instance Foldable Neighborhood where
  foldMap f (Neighborhood []) = mempty
  foldMap f (Neighborhood list) = foldMap f list
-- House
data House = House {location :: Location, state :: State} deriving (Eq)
-- Satisfaction
data Satisfaction = Satisfied | Unsatisfied deriving (Eq)
-- Satisfaction State
data State = B Satisfaction | R Satisfaction | O deriving (Eq)
-- OpenHomes
data OpenHomes a = OpenHomes [a] deriving (Eq)
instance Functor OpenHomes where
  fmap f (OpenHomes []) = OpenHomes []
  fmap f (OpenHomes a) = OpenHomes (fmap f a)

instance Applicative OpenHomes where
  pure = (OpenHomes . pure [])
  (OpenHomes [a]) <*> (OpenHomes [b]) = (OpenHomes ([a] <*> [b]))

instance Monad OpenHomes where
  (>>=) (OpenHomes [a]) f = f a

instance Monoid (OpenHomes a) where
  mempty = OpenHomes []
  (OpenHomes listA) `mappend` (OpenHomes listB) = OpenHomes (listA `mappend` listB)

instance Foldable OpenHomes where
  foldMap f (OpenHomes []) = mempty
  foldMap f (OpenHomes list) = foldMap f list

data ArgsPackage = ArgsPackage {maxRounds :: Int,
                                rows :: Int,
                                cols :: Int,
                                neighRadius :: Int,
                                threshold :: Float,
                                redPer :: Float,
                                bluePer :: Float,
                                emptyPer :: Float}

data SimState = SimState {city :: City (Row House),
                          openHomes :: OpenHomes Location,
                          rad :: Int,
                          thresh :: Float,
                          elaspedTime :: Float} deriving (Eq)

data SimSettings = SimSettings {argsPack :: ArgsPackage,
                                simState :: SimState,
                                roundNum :: Int,
                                stopped :: Bool,
                                stepDelay :: Float}